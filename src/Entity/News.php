<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 * @Gedmo\SoftDeleteable(timeAware=true, hardDelete=false)
 */
class News
{
    use EntityTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\Length(min=3, max=255)
     */
    private $title = '';

    /**
     * @var string
     * @ORM\Column(type="text")
     *
     * @Assert\Length(min=8)
     */
    private $content = '';

    /**
     * @var User $user
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="newsCollection")
     */
    private $user;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}