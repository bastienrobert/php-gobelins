<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity()
 */
class User implements UserInterface
{
    use EntityTrait;

    /**
     * @ORM\Column(unique=true, length=100)
     * @var string|null
     */
    private $username;

    /**
     * @ORM\Column()
     * @var string|null
     */
    private $password;

    /**
     * @var News[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\News", mappedBy="user")
     */
    private $newsCollection;

    /**
     * @var Album[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Album", mappedBy="user")
     */
    private $albumsCollection;

    public function __construct()
    {
        $this->newsCollection = new ArrayCollection();
        $this->albumsCollection = new ArrayCollection();
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return ['ROLE_USER', 'ROLE_ADMIN'];
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    public function getNewsCollection(): ArrayCollection
    {
        return $this->newsCollection;
    }

    public function addNewsCollection(News $news): self
    {
        $this->newsCollection->add($news);
        return $this;
    }

    public function removeNewsCollection(News $news): self
    {
        $this->newsCollection->remove($news);
        return $this;
    }

    public function getAlbumsCollection(): ArrayCollection
    {
        return $this->albumsCollection;
    }

    public function addAlbumsCollection(Album $album): self
    {
        $this->albumsCollection->add($album);
        return $this;
    }

    public function removeAlbumsCollection(Album $album): self
    {
        $this->albumsCollection->remove($album);
        return $this;
    }
}
