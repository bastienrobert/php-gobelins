<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlbumRepository")
 */
class Album
{
    use EntityTrait;
    use TimestampableEntity;

    /**
     * @ORM\Column(type="string", length=80)
     * @Assert\Length(max=80)
     */
    private $title;

    /**
     * @var User $user
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="newsCollection")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Gif", mappedBy="album")
     */
    private $gifs;

    public function __construct()
    {
        $this->gifs = new ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Album
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Album
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Collection|Gif[]
     */
    public function getGifs(): Collection
    {
        return $this->gifs;
    }

    public function addGif(Gif $gif): self
    {
        if (!$this->gifs->contains($gif)) {
            $this->gifs[] = $gif;
            $gif->setAlbum($this);
        }

        return $this;
    }

    public function removeGif(Gif $gif): self
    {
        if ($this->gifs->contains($gif)) {
            $this->gifs->removeElement($gif);
            // set the owning side to null (unless already changed)
            if ($gif->getAlbum() === $this) {
                $gif->setAlbum(null);
            }
        }

        return $this;
    }
}
