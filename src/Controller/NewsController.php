<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NewsController
 * @package App\Controller
 * @Route("/news")
 */
class NewsController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="app_news_index")
     */
    public function index(NewsRepository $newsRepository): Response
    {
        $newsList = $newsRepository->findBy([], ['id' => 'DESC'], 10);

        return $this->render('News/list.html.twig', [
            'form' => $this->createForm(NewsType::class)->createView(),
            'newsList' => $newsList,
        ]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/delete/{news}", requirements={"news"="\d+"})
     */
    public function delete(News $news): Response
    {
        $manager = $this->get('doctrine')->getManager();
        $manager->remove($news);
        $manager->flush();

        return new RedirectResponse($this->generateUrl('app_news_index'));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/create", methods={"POST", "GET"}, name="app_news_create")
     */
    public function create(Request $request): Response
    {
        $form = $this->createForm(NewsType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var News $news */
            $news = $form->getData();
            $news->setUser($this->getUser());
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($news);

            $manager->flush();

            return new RedirectResponse($this->generateUrl('app_news_index'));
        }

        return $this->render('News/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}