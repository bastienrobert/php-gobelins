<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Gif;
use App\Form\AlbumType;
use App\Repository\AlbumRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/album")
 */
class AlbumController extends AbstractController
{
    /**
     * @Route("/", name="album_index", methods="GET")
     */
    public function index(AlbumRepository $albumRepository): Response
    {
        $inSession = $this->get("session")->get('favorites');
        $favorites = [];

        // $this->get("session")->set('favorites', null);

        if ($inSession != null) {
            for($i=0; $i<count($inSession); ++$i) {
                array_push($favorites, $this->getDoctrine()->getRepository(Gif::class)->find($inSession[$i]));
            }
        }

        return $this->render('Album/index.html.twig', [
            'albums' => $albumRepository->findAll(),
            'favorites' => $favorites
        ]);
    }

    /**
     * @Route("/new", name="album_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $album = new Album();
        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $album->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();

            return $this->redirectToRoute('album_index');
        }

        return $this->render('Album/new.html.twig', [
            'Album' => $album,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="album_show", methods="GET")
     */
    public function show(Album $album): Response
    {
        return $this->render('Album/show.html.twig', ['album' => $album]);
    }

    /**
     * @Route("/{id}/edit", name="album_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, Album $album): Response
    {
        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('album_edit', ['id' => $album->getId()]);
        }

        return $this->render('Album/edit.html.twig', [
            'Album' => $album,
            'form' => $form->createView(),
        ]);
    }
}
