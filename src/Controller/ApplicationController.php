<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApplicationController extends AbstractController {
    /**
     * @Route("/", methods={"GET"}, name="root")
     */
    public function index(): Response
    {
        return $this->render('Application/index.html.twig');
    }
}