<?php

namespace App\Controller;

use App\Entity\Gif;
use App\Form\GifType;
use App\Repository\GifRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gif")
 */
class GifController extends AbstractController
{

    /**
     * @Route("/new", name="gif_new", methods="GET|POST")
     * @Security("has_role('ROLE_USER')")
     */
    public function new(Request $request): Response
    {
        $gif = new Gif();
        $form = $this->createForm(GifType::class, $gif);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($gif);
            $em->flush();

            return $this->redirectToRoute('album_index');
        }

        return $this->render('Gif/new.html.twig', [
            'Gif' => $gif,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="gif_edit", methods="GET|POST")
     * @Security("has_role('ROLE_USER')")
     */
    public function edit(Request $request, Gif $gif): Response
    {
        $form = $this->createForm(GifType::class, $gif);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('album_show', ['id' => $gif->getAlbum()->getId()]);
        }

        return $this->render('Gif/edit.html.twig', [
            'gif' => $gif,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/favorite", name="gif_favorite", methods="POST")
     * @Security("has_role('ROLE_USER')")
     */
    public function favorite(Request $request, Gif $gif): Response
    {
        $session = $this->get("session");

        if (in_array($gif->getId(), $session->get('favorites')) == false) {
            if ($session->get('favorites') == null) {
                $session->set('favorites', [$gif->getId()]);
            } else {
                $fav = $session->get('favorites');
                array_push($fav, $gif->getId());
                $session->set('favorites', $fav);
            }
        }

        return $this->redirectToRoute('album_show', ['id' => $gif->getAlbum()->getId()]);
    }

    /**
     * @Route("/{id}", name="gif_delete", methods="DELETE")
     * @Security("has_role('ROLE_USER')")
     */
    public function delete(Request $request, Gif $gif): Response
    {
        if ($this->isCsrfTokenValid('delete'.$gif->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gif);
            $em->flush();
        }

        return $this->redirectToRoute('album_index');
    }
}
